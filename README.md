# BBL-TEST

## Available Scripts

```
npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

```
npm run build
```

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

```
npm run deploy
```

To build you project and run from docker-desktop on you local machine

**Docker desktop installation is require on you local machine**

Dockerfile will use for crete the production build for this project running by serve -s build to run as default [http://localhost:5000](http://localhost:5000)

See more detail about [Production Deployment](https://create-react-app.dev/docs/deployment/)

## Any Further Information

Please send me the e-mail somsakra@live.com

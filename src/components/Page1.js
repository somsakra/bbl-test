import axios from 'axios'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { setApiKey, setValues } from '../actions/appAction'

function Page1() {
  const history = useHistory()
  const { apiKey, values } = useSelector(({ appReducer }) => appReducer)
  const dispatch = useDispatch()

  const getApiKey = async () => {
    try {
      const { data } = await axios.get(process.env.REACT_APP_API_URL + 'key')
      return data.key
    } catch (err) {
      console.log(err)
    }
  }

  const getValue = async (apikey) => {
    const instance = axios.create({
      baseURL: process.env.REACT_APP_API_URL + 'value',
      headers: { authorization: apikey }
    })
    try {
      const { data } = await instance.get()
      return data.value
    } catch (err) {
      console.log(err)
    }
  }

  const handleClickLoad = async () => {
    try {
      const apiKey = await getApiKey()
      const newValues = await getValue(apiKey)
      dispatch(setApiKey(apiKey))
      dispatch(setValues(newValues))
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <div style={{ margin: 50 }}>
      <h1>Test</h1>
      {values.map((value) => (
        <p key={Date.now() + Math.random()}>{value}</p>
      ))}
      {apiKey ? (
        <button onClick={() => history.push('/page2')}>Calculate</button>
      ) : (
        <button onClick={handleClickLoad}>Load</button>
      )}
    </div>
  )
}

export default Page1

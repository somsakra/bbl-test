import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { clearValues } from '../actions/appAction'

function Page2() {
  const { apiKey, values } = useSelector(({ appReducer }) => appReducer)
  const dispatch = useDispatch()
  const summaryResult = values.reduce((acc, val) => acc + val, 0)
  const history = useHistory()
  const handleClickBack = () => {
    dispatch(clearValues())
    history.push('/')
  }

  return (
    <div style={{ margin: 50 }}>
      <p>Key : {apiKey}</p>
      <p>Value : {summaryResult}</p>
      <button onClick={handleClickBack}>Back</button>
    </div>
  )
}

export default Page2

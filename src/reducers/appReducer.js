import {
  APP_ACTION_CLEAR_VALUE,
  APP_ACTION_SET_API_KEY,
  APP_ACTION_SET_VALUES
} from '../Constants'

const initialState = {
  apiKey: null,
  values: []
}

const appReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case APP_ACTION_SET_API_KEY:
      return { ...state, apiKey: payload }
    case APP_ACTION_SET_VALUES:
      return { ...state, values: payload }
    case APP_ACTION_CLEAR_VALUE:
      return initialState

    default:
      return state
  }
}

export default appReducer

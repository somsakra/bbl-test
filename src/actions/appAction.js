import {
  APP_ACTION_CLEAR_VALUE,
  APP_ACTION_SET_API_KEY,
  APP_ACTION_SET_VALUES
} from '../Constants'

export const setApiKey = (payload) => ({
  type: APP_ACTION_SET_API_KEY,
  payload
})

export const setValues = (payload) => ({
  type: APP_ACTION_SET_VALUES,
  payload
})

export const clearValues = () => ({
  type: APP_ACTION_CLEAR_VALUE
})

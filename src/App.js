import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Page1 from './components/Page1'
import Page2 from './components/Page2'

function App() {
  return (
    <div>
      <Router>
        <div>
          <Switch>
            <Route path="/page2">
              <Page2 />
            </Route>
            <Route path="/">
              <Page1 />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  )
}

export default App

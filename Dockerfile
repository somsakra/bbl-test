FROM node:16-alpine

WORKDIR /app

COPY package.json ./
RUN npm install
RUN npm install serve -g

COPY . ./

RUN npm run build

CMD ["serve", "-s" ,"build"]